<?php
include './templates/common/header.php';
require './libs/Login.php';
$email = $password = "";
$emailErr = $passwordErr = "";
$userResult = array();
$validater = 1;

$Login = new Login();

if (isset($_POST['login'])) {


    if ($_POST['email'] == '') {
        $validater = 0;
        $emailErr = "Please enter your email";
    } else {
        $email = $_POST['email'];

        if (filter_var($email, FILTER_VALIDATE_EMAIL)) {
        } else {
            $validater = 0;
            $emailErr = 'Please  Valid your  Email';
        }
    }

    if ($_POST['password'] == '') {
        $validater = 0;
        $passwordErr = 'Please enter your password';
    } else {
        $password = $_POST['password'];
    }

    if ($validater == 1) {
        $Login->doLogin($_POST);
    }
}

?>
<section class="vh-100 m-5">
    <div class="container py-5 w-75 h-100">
        <div class="row d-flex align-items-center justify-content-center h-100">
            <div class="col-md-8 col-lg-7 col-xl-6">
                <img src="./assest/images/school_img.jpg" class="img-fluid" alt="Phone image">
            </div>
            <div class="col-md-7 col-lg-5 col-xl-5 offset-xl-1">
                <form action="#" method="POST">
                    <!-- Email input -->
                    <div class="form-outline mb-4">
                        <label class="form-label" for="form1Example13">Email address</label>
                        <input type="text" id="form1Example13" class="form-control form-control-lg" name="email" value="<?php echo $email ?>" />
                        <span class="red"><?php echo $emailErr ?></span>

                    </div>
                    <!-- Password input -->
                    <div class="form-outline mb-4">
                        <label class="form-label" for="form1Example23">Password</label>
                        <input type="password" id="form1Example23" class="form-control form-control-lg" name="password" value="<?php echo $password ?>" />
                        <span class="red"><?php echo $passwordErr ?></span>

                    </div>

                    <!-- Submit button -->
                    <button type="submit" class="btn btn-primary btn-lg btn-block" name="login">Login</button>
                </form>
            </div>
        </div>
    </div>
</section>