<?php
session_start();
$menu = 'profile';

include "./templates/common/admin/header.php";
include "./libs/profile.php";
$Upload = new Upload();

if (isset($_POST['upload'])) {
    $Upload->Upload_img();
}
if (!isset($_SESSION['id'])) {
    header('location:home.php');
}

$sql = "SELECT*FROM users WHERE id='{$_SESSION['id']}'";
$result = $db->conn->query($sql);
$row = $result->fetch_assoc();
// print_r($_SESSION['id']);
$user_class = '';
$image = '';



?>
<!-- End -->

<!-- Main -->
<div class="container-fluid">
    <div class="row">
        <div class="col-md-2 ">
            <?php include "./templates/common/admin/sidebar.php" ?>
        </div>
        <div class="col-md-10">
            <div class="main mt-5">
                <div class="card">
                    <div class="card-body">
                        <div class="row">
                            <div class="col-md-4">
                                <div class="profile-pic-wrapper">
                                    <?php
                                    if ($row['image'] == '') {
                                        $image = './assest/images/students.jpg';
                                    } else {
                                        $image = './assest/images/upload/' . $row['image'];
                                    }
                                    ?>
                                    <img src="<?php echo $image; ?>" style="width:200px;">
                                </div>
                                <h2 class="text-center"><?php echo $row['fname'] ?></h2>
                                <div class="w-100 p-0">
                                    <form method="POST" action="#" enctype="multipart/form-data">
                                        <div class="mb-2">
                                            <input type="file" class="form-control" id="image" aria-describedby="emailHelp" name='image'>
                                        </div>
                                        <button type="submit" name="upload" class="btn btn-primary mt-3 w-30">Upload</button>
                                    </form>
                                </div>
                            </div>
                            <div class="col-md-8">
                                <table>
                                    <tbody>
                                        <h2>Hi <?php echo $row['fname'] ?></h2>
                                        <?php
                                        $users = array();
                                        if (count($users) > 0) {
                                            foreach ($users as $row) {
                                                echo $row['fname'] . "<br>";
                                            }
                                        }
                                        ?>
                                        <tr>
                                            <td>Name</td>
                                            <td>:</td>
                                            <td><?php echo $row['fname'] ?></td>
                                        </tr>
                                        <tr>
                                            <td>Email</td>
                                            <td>:</td>
                                            <td><?php echo $row['email'] ?></td>
                                        </tr>
                                        <tr>
                                            <td>Gender</td>
                                            <td>:</td>
                                            <td><?php echo $row['gender'] ?></td>
                                        </tr>
                                        <tr>
                                            <td>Moblie Number</td>
                                            <td>:</td>
                                            <td><?php echo $row['phone_Number'] ?></td>
                                        </tr>
                                        <tr>
                                            <td>User Role</td>
                                            <td>:</td>
                                            <td><?php echo $row['user_role'] ?></td>
                                        </tr>
                                        <tr>
                                            <?php
                                            if ($_SESSION['user_role'] == 'role_student' || $_SESSION['user_role'] == 'role_staff') { ?>
                                                <td>Student class</td>
                                                <td>:</td>
                                                <?php
                                                if (isset($_POST['user_class']) != 'user_class') {
                                                    $user_class = $_POST ?>

                                                    <td><?php echo $row['user_class'] ?></td>

                                            <?php ['user_class'];
                                                }
                                            }
                                            ?>
                                        </tr>
                                        <tr>
                                            <td>Hobbis</td>
                                            <td>:</td>
                                            <td><?php echo $row['hobbies'] ?></td>
                                        </tr>
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>