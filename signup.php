<?php
include './templates/common/header.php';
require './libs/Signup.php';

$currentDate = date("y-m-d");
$Sign = new Signup();

$fname = $lname = $email = $password = $gender =  $hobbies = $phone_Number = $user_role = $user_class =  '';
$fnameErr = $lnameErr = $emailErr = $passwordErr = $genderErr = $hobbiesErr  = $phone_NumberErr = $user_TypeErr = $user_classErr  =  '';

$validater = 1;
$hbs = array();
if (isset($_POST['submit'])) {

    if ($_POST['fname'] == '') {
        $validater = 0;
        $fnameErr = 'Please enter your frist name';
    } else {
        $fname = $_POST['fname'];
    }

    if ($_POST['lname'] == '') {
        $validater = 0;
        $lnameErr = 'Please enter your last name';
    } else {
        $lname = $_POST['lname'];
    }

    if ($_POST['email'] == '') {
        $validater = 0;
        $emailErr = "Please enter your email";
    } else {
        $email = $_POST['email'];

        if (filter_var($email, FILTER_VALIDATE_EMAIL)) {
        } else {
            $validater = 0;
            $emailErr = 'Please  Valid your  Email';
        }
    }
    $select = mysqli_query($db->conn,  "SELECT * FROM users WHERE email='{$email}'");
    if (mysqli_num_rows($select)) {
        $validater = 0;
        $emailErr = "This is a already exists!";
    }




    if (isset($_POST['gender']) &&  $_POST['gender'] != "") {
        $gender = $_POST['gender'];
    } else {
        $genderErr = "Please enter your gender";
        $validater = 0;
    }

    if ($_POST['hobbies'] == "") {
        $hobbiesErr = "Please enter your hobbies";
        $validater = 0;
    } else {
        $hobbies = $_POST['hobbies'];
    }



    function validate_mobile($phone_Number)
    {
        return preg_match('/^[0-9]{10}+$/', $phone_Number);
    }
    $phone_Number = $_POST['phone_Number'];

    if (!validate_mobile($phone_Number)) {
        $validater = 0;
        $phone_NumberErr = 'enter valid phonenum';
    } else {
        $phone_Number = $_POST['phone_Number'];
    }

    // $password = $_POST['password'];
    // $uppercase = preg_match('@[A-Z]@', $password);
    // $lowercase = preg_match('@[a-z]@', $password);
    // $number    = preg_match('@[0-9]@', $password);
    // $specialchars = preg_match('@[^\w]@', $password);

    // if (!$uppercase || !$lowercase || !$number || !$specialchars || strlen($password) < 8) {
    //     $passwordErr = 'This is weak password please enter a strong password';
    //     $validater = 0;
    // } else {
    //     $password = ($_POST['password']);
    // }

    if ($_POST['password'] == '') {
        $passwordErr = "Please enter your password";
        $validater = 0;
    } else {
        $password = $_POST['password'];
    }

    if ($_POST['user_role'] == '') {
        $validater = 0;
        $user_TypeErr = 'Please enter your User';
    } else {
        $user_role = $_POST['user_role'];
    }

    if ($_POST['user_class'] == 'user_class') {
        $user_classErr = 'Please enter your User Class';
        $validater = 0;
    } else {
        $user_class = $_POST['user_class'];
    }

    if ($validater == 1) {
        $Sign->doSignup($_POST);
    }
}
?>
<div class="container register">
    <div class="row">
        <div class="col-md-3 mt-5">
            <img src="./assest/images/school_img.jpg" class="img-fluid" alt="Phone image">
            <h5 class="text-white text-center mt-4">School Life
                <a href="login.php" class="text-muted">login</a>
            </h5>
        </div>
        <div class="col-md-9 register-right">
            <div class="tab-content" id="myTabContent">
                <div class="tab-pane fade show active" id="home" role="tabpanel" aria-labelledby="home-tab">
                    <h3 class="register-heading">SIGNUP</h3>
                    <div class="row register-form">
                        <div class="col-md-6">
                            <form action="#" method="post">
                                <div class="form-group">
                                    <input type="text" class="form-control" value="<?php echo $fname ?>" placeholder=" First name" name="fname">
                                    <span class="red"><?php echo $fnameErr ?></span>
                                </div>

                                <div class="form-group">
                                    <input type="text" name="email" class="form-control" placeholder="Your Email *" value="<?php echo $email ?>" />
                                    </span>
                                    <span class="red"><?php echo $emailErr ?></span>
                                </div>
                                <div class="form-group">
                                    <input type="tel" name="phone_Number" id="phonenum" class="form-control" placeholder="Your number *" value="<?php $phone_Number ?>" />
                                    <span class="red"><?php echo $phone_NumberErr ?></span>
                                </div>
                                <div class="form-group">
                                    <h5>Gender</h5><br>
                                    <div class="maxl">
                                        <label class="radio inline">
                                            <input type="radio" name="gender" value="male">
                                            Male
                                        </label><br>
                                        <label class="radio inline">
                                            <input type="radio" name="gender" value="female">
                                            <span>Female </span>
                                        </label><br>
                                        <label class="radio inline">
                                            <input type="radio" name="gender" value="Other">
                                            <span>Other</span>
                                        </label>
                                        <span class="red"><?php echo $genderErr ?></span>
                                    </div>
                                </div>
                        </div>
                        <input type="hidden" value="1" name="status">
                        <div class="col-md-6">
                            <div class="form-group">
                                <input type="text" class="form-control" name="lname" placeholder="Last Name *" value="<?php echo $lname ?>" />
                                <span class="red"><?php echo $lnameErr ?></span>
                            </div>
                            <div class="form-group">
                                <input type="password" class="form-control" placeholder="Password *" value="<?php echo $password ?>" name="password" />
                                <span class="red"><?php echo $passwordErr ?></span>
                            </div>
                            <!--  -->
                            <div class="row">
                                <select id="user_role" name="user_role" class="form-control">
                                    <option>Select User...</option>
                                    <option value="role_admin">role_admin</option>
                                    <option value="role_staff">role_staff</option>
                                    <option value="role_student">role_student</option>
                                </select><br>
                                <span class="red"><?php echo $user_TypeErr ?></span>
                                <div class="w-100 mt-2" id="student_classes" style="display:none">

                                    <select class="form-control" id="user_class" name="user_class" value=<?php echo $user_class ?>>
                                        <option value="">Select Student Class...</option>
                                        <option value="1">1st</option>
                                        <option value="2">2nd</option>
                                        <option value="3">3rh</option>
                                        <option value="4">4th</option>
                                        <option value="5">5th</option>
                                        <option value="6">6th</option>
                                        <option value="7">7th</option>
                                        <option value="8">8th</option>
                                        <option value="9">9th</option>
                                        <option value="10">10th</option>
                                        <option value="11">+1th</option>
                                        <option value="12">+2th</option>
                                    </select>
                                    <span class="red"><?php echo $user_classErr ?></span>
                                </div>
                            </div>
                            <!--  -->
                            <div class="form-group">
                                <div class="maxl mt-4">
                                    <h5>Hobbies</h4><br>
                                        <label class="radio inline">
                                            <input type="checkbox" name="hobbies[]" value="Cricket">
                                            Cricket
                                        </label>
                                        <label class="radio inline">
                                            <input type="checkbox" name="hobbies[]" value="Football">
                                            <span>Football </span>
                                        </label><br>
                                        <label class="radio inline">
                                            <input type="checkbox" name="hobbies[]" value="Basketball">
                                            Basketball
                                        </label>
                                        <label class="radio inline">
                                            <input type="checkbox" name="hobbies[]" value="hockey">
                                            <span>Hockey</span><br>
                                        </label>
                                        <span class="red"><?php echo $hobbiesErr ?></span>
                                </div>
                            </div>
                            <input type="submit" name="submit" class="btnRegister" value="Register" />
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>