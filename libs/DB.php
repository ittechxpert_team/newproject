<?php
class DB
{
    public $conn = "";
    public $host = "localhost";
    public $user = "root";
    public $password = "";
    public $db = "new_school";

    function __construct()
    {
        $this->conn = new mysqli($this->host, $this->user, $this->password, $this->db);
        if ($this->conn->connect_error) {
            die("Connent fales" . $this->conn->connect_error);
        }
    }

    public function insert($sql)
    {
        return $this->conn->query($sql);
    }

    public function select($sql)
    {
        return $this->conn->query($sql);
    }
    public function update($sql)
    {
        return $this->conn->query($sql);
    }
    public function Upload_img($sql)
    {
        return $this->conn->query($sql);
    }
}
