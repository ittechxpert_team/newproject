<?php
include "DB.php";
$db = new DB();

class Upload
{
    public $db;
    function __construct()
    {
        $this->db = new DB();
    }
    function Upload_img()
    {
        if (isset($_POST['upload'])) {
            $filename = $_FILES["image"]["name"];
            $tempname = $_FILES["image"]["tmp_name"];
            $folder = "./assest/images/upload/" . $filename;
            $sql = "UPDATE `users` SET `image` = '{$filename}' WHERE `id` = {$_SESSION['id']}";

            mysqli_query($this->db->conn, $sql);
            if (move_uploaded_file($tempname, $folder)) {
                echo "<h6 class='text-center alert alert-success'>  Image uploaded successfully!</h6 >";
            } else {
                echo "<h6 class='text-center alert alert-danger'>  Failed to upload image!</h6>";
            }
        }
    }
}
