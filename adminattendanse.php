<?php
include "./templates/common/admin/header.php";
include './libs/Users.php';
$menu = 'adminattendanse.php';
$db = new DB();
$tab = new Users();
if (!isset($_SESSION['id'])) {
    header('location:home.php');
}
$currentClass = 1;
if (isset($_GET['cid'])) {
    $currentClass = $_GET['cid'];
}
?>
<style>
    a.btn.btn-dark {
        margin: 14px 13px;
    }
</style>
<div class="container-fulid ">
    <div class="row w-100 d-flex">
        <div class="col-md-2">
            <?php include './templates/common/admin/sidebar.php'; ?>
        </div>
        <div class="col-md-10">
            <ul>
                <?php
                foreach ($_SESSION['classes'] as $class) {
                    echo '<a class="btn btn-dark" href="adminattendanse.php?cid=' . $class['id'] . '">' . $class['title'] . '</a>';
                }
                ?>
            </ul>
            <?php if (isset($_REQUEST['date'])) { ?>
                <a class="btn btn-dark mt-4 " href="adminattendanse.php">Back</a>
            <?php } ?>
            <div class="CSSTableGenerator">
                <table>
                    <?php
                    $select = "SELECT count(id)
                    total_student,student_id,  status,class,added_at FROM `attendanse` WHERE class=" . $currentClass . " GROUP BY added_at";
                    if (isset($_REQUEST['date'])) {

                        $select = "SELECT att.id,att.student_id, att.status,att.comment,att.class,att.added_at,u.fname,u.lname,u.image FROM `attendanse` att JOIN users u ON att.student_id = u.id WHERE  class=" . $currentClass . " AND att.added_at = '" . $_REQUEST['date'] . "' ";
                    }
                    $query = $db->conn->query($select);

                    ?>

                    <?php if (!isset($_REQUEST['date'])) { ?>
                        <tr>
                            <td>Sno.</td>
                            <td>total_student</td>

                            <td>added_at</td>
                            <td>Action </td>
                        </tr>
                    <?php } else { ?>
                        <tr>
                            <td>Image</td>
                            <td>Name</td>
                            <td>status</td>
                            <td>Leave Reason</td>
                            <td>added_at</td>
                            <td>Action </td>
                        </tr>
                    <?php } ?>
                    <?php


                    $sno = 1;
                    while ($row = mysqli_fetch_assoc($query)) {
                    ?>
                        <tr>
                            <td>
                                <?php
                                if (!isset($_REQUEST['date'])) {
                                    echo $sno;
                                } else {
                                    if ($row['image'] == '') {
                                        $image = "assest/images/no-img.jpg";
                                        echo $image;
                                    } else {
                                        $image = 'assest/images/upload/' . $row['image'];
                                    } ?>
                                    <img src="<?php echo $image; ?>" style="width:80px;height:80px;border-radius:10%;object-fit:cover;">
                                <?php
                                } ?>
                            </td>
                            <?php if (!isset($_REQUEST['date'])) { ?>
                                <td><?php echo $row['total_student'] ?></td>
                            <?php } ?>
                            <?php if (isset($_REQUEST['date'])) { ?>
                                <td><?php echo $row['fname'] . ' ' . $row['lname'] ?></td>
                                <td><?php echo $row['status'] ?></td>
                                <td><?php echo $row['comment'] ?></td>

                            <?php } ?>

                            <td><?php echo $row['added_at'] ?></td>

                            <td>
                                <?php if (!isset($_REQUEST['date'])) { ?>
                                    <a class="btn btn-dark" href="adminattendanse.php?cid=<?= $currentClass; ?>&date=<?php echo $row['added_at'] ?>"><i class="fa fa-eye "></i></a>
                                <?php } else { ?>

                                    <a class="btn btn-success" href="att_list.php?id=<?php echo $row['id'] ?>"><i class="fa fa-pen"></i></a>
                                <?php } ?>

                            </td>
                        </tr>
                    <?php
                        $sno++;
                    }
                    ?>
                </table>
            </div>
        </div>
    </div>
</div>