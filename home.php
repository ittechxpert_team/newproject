<?php
include "./libs/profile.php";
include './templates/common/header.php';
?>

<head>
    <style>
        .row-w {
            width: 100%;
            height: 700px;
            /* overflow: auto; */
        }
    </style>

</head>
<div id="carouselExampleIndicators" class="carousel slide row-w" data-ride="carousel ">
    <div class="carousel-indicators">
        <li data-target="#carouselExampleIndicators" data-slide-to="0" class="active"></li>
        <li data-target="#carouselExampleIndicators" data-slide-to="1"></li>
        <li data-target="#carouselExampleIndicators" data-slide-to="2"></li>
        <li data-target="#carouselExampleIndicators" data-slide-to="3" class="active"></li>
        <li data-target="#carouselExampleIndicators" data-slide-to="4"></li>
        <li data-target="#carouselExampleIndicators" data-slide-to="5"></li>
    </div>

    <div class="carousel-inner">
        <div class="carousel-item active">
            <img class="d-block w-100 img" src="./assest/images/Kids-Education-Apps.jpg" alt="First slide">
        </div>
        <div class="carousel-item">
            <img class="d-block w-100 img" src="./assest/images/image-2.jpg" alt="Second slide">
        </div>
        <div class="carousel-item">
            <img class="d-block w-100 img" src="./assest/images/img2.webp" alt="Third slide">
        </div>
        <div class="carousel-item">
            <img class="d-block w-100 img" src="./assest/images/img.webp" alt="Third slide">
        </div>
        <div class="carousel-item">
            <img class="d-block w-100 img" src="./assest/images/school_img.jpg" alt="Third slide">
        </div>
    </div>
    <a class="carousel-control-prev" href="#carouselExampleIndicators" role="button" data-slide="prev">
        <span class="carousel-control-prev-icon" aria-hidden="true"></span>
        <span class="sr-only">Previous</span>
    </a>
    <a class="carousel-control-next" href="#carouselExampleIndicators" role="button" data-slide="next">
        <span class="carousel-control-next-icon" aria-hidden="true"></span>
        <span class="sr-only">Next</span>
    </a>
    <?php
    ?>

    <?php
    include './templates/common/card.php';
    include './templates/common/footer.php';
    ?>
</div>
<script src="https://code.jquery.com/jquery-3.2.1.slim.min.js" integrity="sha384-KJ3o2DKtIkvYIK3UENzmM7KCkRr/rE9/Qpg6aAZGJwFDMVNA/GpGFF93hXpG5KkN" crossorigin="anonymous">
</script>
<script src="https://cdn.jsdelivr.net/npm/popper.js@1.12.9/dist/umd/popper.min.js" integrity="sha384-ApNbgh9B+Y1QKtv3Rn7W3mgPxhU9K/ScQsAP7hUibX39j7fakFPskvXusvfa0b4Q" crossorigin="anonymous">
</script>
<script src="https://cdn.jsdelivr.net/npm/bootstrap@4.0.0/dist/js/bootstrap.min.js" integrity="sha384-JZR6Spejh4U02d8jOt6vLEHfe/JQGiRRSQQxSfFWpi1MquVdAyjUar5+76PVCmYl" crossorigin="anonymous">
</script>