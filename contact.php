<?php
include './templates/common/header.php';

?>

<div class="container register">
    <div class="row">
        <div class="col-md-3 register-left">
        </div>
        <div class="col-md-9 register-right">

            <div class="tab-content" id="myTabContent">
                <div class="tab-pane fade show active" id="home" role="tabpanel" aria-labelledby="home-tab">
                    <h3 class="register-heading">Contact As</h3>
                    <div class="row register-form">
                        <div class="col-md-6">
                            <form action="#" method="POST">
                                <div class="form-group">
                                    <input type="text" name="name" class="form-control" placeholder="Your name *" value="" />
                                    <span class="red"></span>
                                </div>
                                <div class="form-group">
                                    <select id="country" name="country" class="form-control">
                                        <option value="usa">India</option>
                                        <option value="australia">Australia</option>
                                        <option value="canada">Canada</option>
                                        <option value="usa">USA</option>
                                    </select>
                                    <span class="red"></span>
                                </div>
                        </div>
                        <div class="col-md-6">

                            <div class="form-group">
                                <input type="text" class="form-control" placeholder="Enter your  email" value="" name="email" />
                                <span class="red"></span>
                            </div>

                            <div class="form-group">
                                <textarea id="subject" class="form-control" name="subject" placeholder="Write something.." style="height:50px"></textarea>
                            </div>
                            <input type="submit" name="submit" class="btnRegister" value="Contact" />
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>