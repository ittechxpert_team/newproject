<?php
session_start();
?>
<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Document</title>
    <!-- <link rel='stylesheet' href='../../assest/css/bootstrap.min.css'> -->
    <link href="//maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css" rel="stylesheet" id="bootstrap-css">
    <script src="//maxcdn.bootstrapcdn.com/bootstrap/4.0.0/js/bootstrap.min.js"></script>
    <script src="//code.jquery.com/jquery-1.11.1.min.js"></script>
    <!------ Include the above in your HEAD tag ---------->
    <link href="//maxcdn.bootstrapcdn.com/font-awesome/4.2.0/css/font-awesome.min.css" rel="stylesheet">
    <!--  -->
    <!--  -->
    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.7.0/css/all.css" integrity="sha384-lZN37f5QGtY3VHgisS14W3ExzMWZxybE1SJSEsQp9S+oqd12jhcu+A56Ebc1zFSJ" crossorigin="anonymous">
    <link rel="stylesheet" href="./assest/css/contact.css">
    <link rel="stylesheet" href="../../assest/css/signup.css">
    <link rel="stylesheet" href="./assest/css/footer.css">
    <link rel="stylesheet" href="./assest/css/header.css">
    <link rel="stylesheet" href="././assest/css/about.css">
    <link rel="stylesheet" href="././assest/css/card.css">
    <link rel="stylesheet" href="././assest/css/reading.css">

    <script src="././assest/js/script.js"></script>
    <script src="./././assest/js/index.js"></script>


</head>

<body>
    <div class="fixed-top">
        <nav class="navbar navbar-expand-lg navbar-dark mx-background-top-linear">
            <div class="container">
                <a class="navbar-brand" rel="nofollow" href="#" style="text-transform: uppercase;">School </a>
                <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarResponsive" aria-controls="navbarResponsive" aria-expanded="false" aria-label="Toggle navigation">
                    <span class="navbar-toggler-icon"></span>
                </button>
                <div class="collapse navbar-collapse" id="navbarResponsive">

                    <ul class="navbar-nav ml-auto">

                        <li class="nav-item active">
                            <a class="nav-link" href="home.php">Home
                            </a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link" href="contact.php">contact As</a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link" href="about.php">About As</a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link" href="reading.php">Learn</a>
                        </li>
                        <?php
                        if (isset($_SESSION['id'])) {
                        ?>
                            <li class=" nav-item">
                                <a class="nav-link" href="logout.php">Logout</a>
                            </li>
                        <?php } else { ?>

                            <li class=" nav-item">
                                <a class="nav-link" href="signup.php">Signup</a>
                            </li>
                            <li class="nav-item">
                                <a class="nav-link" href="login.php">Login</a>
                            </li>
                        <?php } ?>
                    </ul>
                </div>
            </div>
        </nav>
    </div>