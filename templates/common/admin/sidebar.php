<?php
?>
<div class="page-wrapper chiller-theme toggled">
  <nav id="sidebar" class="sidebar-wrapper">
    <div class="sidebar-content bg-dark">
      <div class="sidebar-menu">
        <ul>
          <?php
          if ($_SESSION['user_role'] == 'role_admin' || $_SESSION['user_role'] == 'role_staff') {

          ?>
            <li class="sidebar-dropdown <?php echo ($menu == 'dashboard') ? 'active' : '' ?>">
              <a href="dashboard.php">
                <i class="fa fa-home"></i>
                <span>Dashboard</span>
              </a>
            </li>
          <?php } ?>
          <li class="sidebar-dropdown <?php echo ($menu == 'profile') ? 'active' : '' ?>">
            <a href="profile.php">
              <i class="fa fa-address-card"></i>
              <span>Profile</span>
            </a>
          </li>
          <?php

          if ($_SESSION['user_role'] != 'role_admin') { ?>
            <li class="sidebar-dropdown <?php echo ($menu == 'my_class') ? 'active' : '' ?>">
              <a href="my_class.php">
                <i class="fa fa-address-card"></i>
                <span>My class</span>
              </a>
            </li>
          <?php
          }
          if ($_SESSION['user_role'] != 'role_admin') { ?>
            <li class="sidebar-dropdown <?php echo ($menu == 'attend_update') ? 'active' : '' ?>">
              <a href="attend_update.php">
                <i class="fa fa-address-card"></i>
                <span>Attend List</span>
              </a>
            </li>
          <?php
          }
          ?>
          <?php
          if ($_SESSION['user_role'] == 'role_admin' || $_SESSION['user_role'] == '') { ?>
            <li class="sidebar-dropdown <?php echo ($menu == 'adminattendanse.php') ? 'active' : '' ?>">
              <a href="adminattendanse.php">
                <i class="fa fa-address-card"></i>
                <span>Attend</span>
              </a>
            </li>
          <?php
          }
          ?>
          <li class="sidebar-dropdown <?php echo ($menu == 'role_student') ? 'active' : '' ?>">
            <a href="user_list.php?role=role_student">
              <i class="fa fa-tachometer-alt"></i>
              <span>Student</span>
            </a>
            <div class="sidebar-submenu " <?php echo ($menu == 'role_student') ? "style='display:block;'" : '' ?>>
              <ul>
                <?php
                foreach ($_SESSION['classes'] as $class) { ?>
                  <li class="<?php echo ($class['id'] == $submenu && $menu == 'role_student') ? 'active' : ''; ?>">
                    <a href="user_list.php?role=role_student&class=<?php echo $class['id'] ?>"><?php echo $class['title']; ?></a>
                  </li>
                <?php }
                ?>
              </ul>
            </div>
          </li>
          <li class="sidebar-dropdown <?php echo ($menu == 'role_staff') ? 'active' : '' ?>">
            <a href="user_list.php?role=role_staff">
              <i class="fa fa-tachometer-alt"></i>
              <span>Staff</span>
            </a>
            <div class="sidebar-submenu" <?php echo ($menu == 'role_staff') ? "style='display:block;'" : '' ?>>
              <ul>
                <?php
                foreach ($_SESSION['classes'] as $class) { ?>
                  <li class="<?php echo ($class['id'] == $submenu && $menu == 'role_staff') ? 'active' : ''; ?>">
                    <a href="user_list.php?role=role_staff&class=<?php echo $class['id'] ?>"><?php echo $class['title']; ?></a>
                  </li>
                <?php }
                ?>
              </ul>
            </div>
          </li>
        </ul>
      </div>
    </div>
  </nav>
</div>
<script>
  jQuery(function($) {

    $(".sidebar-dropdown > a").click(function() {
      $(".sidebar-submenu").slideUp(200);
      if (
        $(this)
        .parent()
        .hasClass("active")
      ) {
        $(".sidebar-dropdown").removeClass("active");
        $(this)
          .parent()
          .removeClass("active");
      } else {
        $(".sidebar-dropdown").removeClass("active");
        $(this)
          .next(".sidebar-submenu")
          .slideDown(200);
        $(this)
          .parent()
          .addClass("active");
      }
    });

    $("#close-sidebar").click(function() {
      $(".page-wrapper").removeClass("toggled");
    });
    $("#show-sidebar").click(function() {
      $(".page-wrapper").addClass("toggled");
    });

  });
</script>