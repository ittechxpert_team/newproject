<?php
?>
<!DOCTYPE html>
<html lang="en">

<head>
    <title>Bootstrap Example</title>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <script src="//code.jquery.com/jquery-1.11.1.min.js"></script>
    <link href="//maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css" rel="stylesheet" id="bootstrap-css">
    <script src="//maxcdn.bootstrapcdn.com/bootstrap/4.0.0/js/bootstrap.min.js"></script>
    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.7.0/css/all.css" integrity="sha384-lZN37f5QGtY3VHgisS14W3ExzMWZxybE1SJSEsQp9S+oqd12jhcu+A56Ebc1zFSJ" crossorigin="anonymous">

    <!--  -->
    <link rel="stylesheet" type="text/css" href="https://pixinvent.com/stack-responsive-bootstrap-4-admin-template/app-assets/css/bootstrap-extended.min.css">
    <link rel="stylesheet" type="text/css" href="https://pixinvent.com/stack-responsive-bootstrap-4-admin-template/app-assets/fonts/simple-line-icons/style.min.css">

    <!--  -->
    <link rel="stylesheet" href="./././assest/css/user.css">
    <link rel="stylesheet" href="./././assest/css/profile.css">
    <link rel="stylesheet" href="./././assest/css/style.css">
    <link rel="stylesheet" href="./././assest/css/list.css">
    <link rel="stylesheet" href="./././assest/css/card.css">
    <link rel="stylesheet" href="./././assest/css/footer.css">
    <link rel="stylesheet" href="./../../assest/js/index.js">

    <script src="./././assest/js/ascript.js"></script>


    <style>
        .left-menu {
            padding: 0px;
        }

        .left-menu li {
            padding: 10px;
            border-bottom: 1px solid;
        }
    </style>
</head>


<body>
    <nav class="navbar navbar-expand-sm bg-dark justify-content-between position-relative">
        <ul class="navbar-nav">
            <li class="nav-item active">
                <a class="nav-link" href="home.php">TVN School</a>
            </li>
            <li class="nav-item ">

            </li>
        </ul>
        <ul class="navbar-nav">
            <li class="nav-item">
                <a class="nav-link" href="logout.php">Logout</a>
            </li>
        </ul>
    </nav>