<?php
$menu = 'attend_update';

include "./templates/common/admin/header.php";
include "./libs/Users.php";
$USER = new Users();
$db = new DB();

if (!isset($_SESSION['id'])) {
    header('location:home.php');
}
$sql = "SELECT*FROM users WHERE id='{$_SESSION['id']}'";
$result = $db->conn->query($sql);
$row = $result->fetch_assoc();
?>
<style>
    .shadow {
        text-align: center;
        margin: 10px;
    }
</style>
<div class="container-fulid">
    <div class="row">
        <div class="col-md-2">
            <?php include "./templates/common/admin/sidebar.php" ?>
        </div>
        <div class="col-md-10 mt-4">
            <h2 class="shadow"><?php echo $row['user_class'] ?> Class </h2>
            <?php
            if (isset($_REQUEST['date'])) { ?>

                <div class="mt-2 ">
                    <a href="attend_update.php" class="btn btn-primary">Back</a>
                </div>
            <?php
            }
            ?>
            <div class="col">
                <div class="CSSTableGenerator">
                    <table>
                        <?php
                        $select = "SELECT count(id)
                    total_student,student_id,  status,class,added_at FROM `attendanse` WHERE staff_id=" . $_SESSION['id'] . " AND class=" . $_SESSION['user_class'] . " GROUP BY added_at";

                        if (isset($_REQUEST['date'])) {
                            $select = "SELECT id,student_id, status,class,added_at FROM `attendanse` WHERE staff_id=" . $_SESSION['id'] . " AND class=" . $_SESSION['user_class'] . " AND added_at = '" . $_REQUEST['date'] . "' ";
                        }
                        $query = $db->conn->query($select);
                        ?>
                        <?php
                        if (!isset($_REQUEST['date'])) {
                        ?>
                            <tr>
                                <td>Sno.</td>
                                <td>Tole Student</td>
                                <td>Added_at</td>
                                <td>Action</td>
                            </tr>
                        <?php } else { ?>
                            <tr>
                                <td>Sno.</td>
                                <td>student_id</td>
                                <td>status</td>
                                <td>Added_at</td>
                                <td>Action</td>
                            </tr>
                        <?php } ?>
                        <?php
                        $sno = 1;
                        while ($row = $query->fetch_assoc()) {
                        ?>
                            <tr>
                                <td><?php echo $sno; ?>
                                </td>
                                <?php
                                if (!isset($_REQUEST['date'])) { ?>
                                    <td><?php echo $row['total_student'] ?>
                                    </td>
                                <?php } ?>
                                <?php
                                if (isset($_REQUEST['date'])) {
                                ?>
                                    <td><?php echo $row['student_id'] ?>
                                    </td>
                                    <td><?php echo $row['status']
                                        ?></td>
                                <?php } ?>

                                </td>
                                </td>
                                <td><?php echo $row['added_at']
                                    ?></td>
                                <td>
                                    <?php
                                    if (!isset($_REQUEST['date'])) { ?>
                                        <a class="btn btn-dark" href="attend_update.php?date=<?php echo $row['added_at'] ?>"><i class="fa fa-eye "></i></a>
                                    <?php } else { ?>
                                        <a class="btn btn-success" href="att_list.php?&id=<?php echo $row['id'] ?>"><i class="fa fa-pen"></i></a>
                                    <?php } ?>
                                </td>
                            </tr>
                        <?php
                            $sno++;
                        } ?>
                    </table>
                </div>
            </div>
        </div>
    </div>
</div>