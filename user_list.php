<?php
$menu = 'user_list';

include "./templates/common/admin/header.php";
include "./libs/Users.php";
$menu = $_GET['role'];
if (isset($_REQUEST['class'])) {
    $submenu = $_REQUEST['class'];
}

$USER = new Users();
if (!isset($_SESSION['id'])) {
    header('location:home.php');
}

if (isset($_GET['id'])) {
    $USER->Delete_id($_GET);
}

?>

<head>
    <style>
        .alert {
            width: 220px;
            /* margin: auto; */
            display: flex;
            align-items: center;
        }
    </style>
</head>
<div class="container-fulid">
    <div class="row">
        <div class="col-md-2">
            <?php include "./templates/common/admin/sidebar.php" ?>
        </div>
        <div class="col-md-10">
            <?php
            if (isset($_GET['role']) && isset($_GET['class'])) {
                $role = $_GET['role'];
                $class = $_GET['class'];
                $users = $USER->getUsersByRoleAndClass($role, $class);
            } else {
                $role = $_GET['role'];
                $users = $USER->getUsersByRole($role);
            }
            if (count($users) == 0) { ?>
                <div class=" alert text-white   text-center bg-dark mt-5 " role="alert">
                    <?php echo "User Not exist for this class"; ?>
                </div>
            <?php } else {
            ?>
                <div class="col">
                    <div class="CSSTableGenerator">
                        <table>
                            <tr>
                                <td>ID</td>
                                <td>Frist Name</td>
                                <td>Last Name</td>
                                <td>Email</td>
                                <td>User Role</td>
                                <td>User class</td>
                                <td>Gender</td>
                                <td>Action</td>
                            </tr>
                            <?php
                            foreach ($users as $USER) { ?>
                                <tr>
                                    <td><?php echo $USER['id']; ?>
                                    </td>
                                    <td><?php echo $USER['fname'] ?>
                                    </td>
                                    <td><?php echo $USER['lname'] ?>
                                    </td>
                                    <td><?php echo $USER['email'] ?>
                                    </td>
                                    <td><?php echo $USER['user_role']
                                        ?></td>
                                    <td><?php echo $USER['user_class']
                                        ?></td>
                                    <td><?php echo $USER['gender']
                                        ?></td>
                                    <td>
                                        <a class="btn btn-success" href="update.php?id=<?php echo $USER['id'] ?>"><i class="fa fa-pen "></i></a>

                                        <a class="btn btn-danger delete-user" href="?role=<?= $_REQUEST['role'] ?>&id=<?php echo $USER['id'] ?>"><i class="fa fa-trash"></i></a>
                                    </td>
                                </tr>
                            <?php } ?>
                        </table>
                    </div>
                </div>
        </div>
    <?php } ?>
    </div>
</div>