<?php
$menu = 'my_class';
include './templates/common/admin/header.php';
include './libs/attendanse.php';
session_start();
$at = new Attend();
$response = array();
$id = $_GET['id'];
if (isset($_REQUEST['submit'])) {
    $at->attupdateUser($_POST);
}
if (!isset($_SESSION['id'])) {
    header('location:home.php');
}
?>

<head>
    <style>
        .row {
            display: flex;
            -ms-flex-wrap: wrap;
            flex-wrap: wrap;
            margin-right: -15px;
            margin-left: -4px;
            justify-content: space-between;
        }

        .col-md-2 {
            -webkit-box-flex: 0;
            -ms-flex: 0 0 16.666667%;
            flex: 0 0 16.666667%;
            max-width: 15.9%;
        }
    </style>
</head>
<div class="container-fulid">
    <div class="row ">
        <div class="col-md-2"><?php include "./templates/common/admin/sidebar.php" ?></div>
        <div class="col-md-10">
            <form action="#" method="POST">
                <div class="row">
                    <?php
                    $sql = "SELECT att.*,u.fname,u.lname FROM `attendanse` att JOIN users u ON att.student_id = u.id WHERE att.`id` = '{$id}'";
                    $result = $db->select($sql);
                    while ($row = mysqli_fetch_assoc($result)) {
                    ?><div class="col-md-2 mt-3">
                            <div class="card  text-black border border-dark  ">
                                <div class="card-body">
                                    <h4><?php echo $row['fname'] ?></h4>
                                    <div class="form-check form-check-inline"><input class="form-check-input attendance" type="radio" name="attend" <?= ($row['status'] == 'Present') ? 'checked' : ''; ?> value="Present" /><label class="form-check-label">Present</label></div>
                                    <div class="form-check form-check-inline"><input class="form-check-input attendance" type="radio" name="attend" <?= ($row['status'] == 'Absent') ? 'checked' : ''; ?> value="Absent" /><label class="form-check-label">Absent</label></div>
                                    <div class="form-check form-check-inline"><input class="form-check-input attendance" type="radio" name="attend" <?= ($row['status'] == 'Leave') ? 'checked' : ''; ?> value="Leave" /><label class="form-check-label">Leave</label></div>
                                    <div class="form-outline leave-message" style="display: <?= ($row['status'] == 'Leave') ? 'block' : 'none'; ?>;"><textarea name="comment" class="form-control rounded-0" placeholder="Reason.." value="<?php echo $comment ?>" style="width: 100%"><?= $row['comment'] ?></textarea></div>
                                </div>
                            </div>
                        </div><?php } ?>
                </div>
                <div class="btn"><input class="btn btn-success " name="submit" type="submit" value="Update" /></div>
            </form>
        </div>
    </div>
</div>
<script>
    $('.attendance').change(function() {
        if ($(this).val() == 'Leave') {
            $(this).parents('.card-body').find('.leave-message').show();
        } else {
            $(this).parents('.card-body').find('.leave-message').hide();
        }
    })
</script>