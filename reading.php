<?php
include './templates/common/header.php';

?>

<head>
    <style>

    </style>
</head>
<main class="mt-5">
    <div class="book">
        <div class="book-cover">
            <div>
                <h1>Routash kumar</h1>
                <div class="separator"></div>
            </div>
        </div>
        <div class="book-content">
            <h3>Best Things About the School Life for Students
            </h3>

            <p>Mr. Routash kumar .</p>

            <p>School Life is the best time of our life as we make new friends, learn new things and build our career there. School time is the only time which we enjoy most, and when we enter college, we always miss our school life. School life teaches us lots of new things and prepares us to face all the challenges of life. I love my school life and really enjoy it. I have lots of friends, and all my teachers love me. I love my school and enjoy going there every day to meet my friends and learn new things.</p>

            <p>Everyone keeps on saying that school life is the best time of your life. When listening to these phrases from their elders, the school students think about what is good about this life. All we do in the entire day is to attend the classes and to do the homework. But once school life gets over, the students realise that school life was the best time of their life.</p>

            <p>Apart from learning great things in school, you make new friends, play different sports and create memories for the rest of your life. The students also learn many life skills like teamwork, good manners, etc., and understand what they want to become in their life.</p>

            <h3>About My School and School Life
            </h3>

            <p>I study in the reputed private school in my city, and I am glad to be a student of this school. My school is one of the most renowned schools in my town. It is very beautiful and huge. My school has all the facilities for sports, study and other activities. Built-in a three-storey building, it is a Co-Ed and Senior Secondary School having Science, Arts and Commerce stream. The atmosphere at my school is delightful. We have a huge playing ground where we all students play different games like Badminton, Basketball, Cricket, etc. We have a separate Basketball and Tennis court, as well as a small and beautiful garden for kids.</p>

            <p>The students practice these sports every day. The school also has a big swimming pool and sports area for indoor games. In this area, the students can play Table Tennis and Chess. There is also a big skating rink. Different physical training teachers train us for all these sports. These sports not only keep us fit but also increase our stamina and coordination.</p>

            <p>School life is not just limited to studies and sports. There are also other activity rooms such as the music room, art room, and dance room. The Art room is a big hall with lots of colourful charts and various types of paints. The students can portray their imagination and can create beautiful art here. Dance and music are also very important in school life as they help the students express themselves in a new manner. The different movements in dance help the students in unwinding themselves.</p>

            <p>My school has a big library where we all read different kinds of books, novels and comics. Apart from the library, my school has well-equipped scientific labs where we all practise various experiments of Chemistry, Physics and Biology. I have learned a lot of things in these labs. My school also has a big computer lab with trained technical staff that help us to learn everything about computers. I love playing on the computer and learning new things in the computer lab. In the computer lab, the students learn about using MS Word and PowerPoint. The computer lab is also called the ICT lab. The ICT teacher teaches the students about the internet and how to use it safely.</p>

            <p>All the staff at my school are very polite, educated and experienced. Our teachers not only teach us but also prepare us for various competitions, and every year, my school wins many prizes in various competitions. I have even represented my school in a hockey championship and scored the second position.</p>

            <p>The classrooms are big and decorated beautifully with different artwork done by the students. Various projects and models are kept in the classroom for the students to keep on revising their concepts. The teachers use smartboards, and every day a new word is taught to enhance the students’ vocabulary. Every day one student presents the ‘Thought for the Day’. These positive thoughts keep us motivated.</p>

            <p>All the teachers at my school are very dedicated and punctual. They always teach us discipline and ask us to come to school on time. Our teachers love us, and they teach us in a very simple and easy way. Whenever we fail to understand anything, they try to make us understand it again without shouting at us. They give equal attention to all the students, and that is why my school has an excellent academic record. </p>

            <p>The teachers are well qualified. They use different techniques to teach us the concepts. The atmosphere in the school is fun-filled and fruitful at the same time. The school’s entire staff, from the security guard to the teachers, is very helpful and polite.</p>

            <p>We learn many life skills also in school. Discipline, hard work, and punctuality are some of them. During sports classes, we learn teamwork and work together to win.</p>

            <p>The best part of my school is its auditorium where all the school events and competitions take place. Our school auditorium is one of the best auditoriums in the town with a great sound and light facility. It is fully air-conditioned with lots of seats. Every year, my school organises an annual cultural festival which lasts for two days. Many cultural events take place within these two days like singing, dancing, debate competition, etc. I love to participate in a poem writing competition every year, and many times I have won prizes as well. Every year, the toppers of our school get awarded on this annual cultural day and we all students love to participate in various events of the annual cultural day.</p>

            <p>We also have school assemblies in our Auditorium. Sometimes the students from other schools come and participate in the different competitions organised in our schools. These events are called inter-school competitions. These events are very good as we get to learn new things from other students and make many new friends.</p>

            <p>Apart from the annual cultural day, my school is famous for organising one of the biggest annual sports meets. I love this annual sports meet because sports are my favourite. In this annual sports meet, various sports competitions are organised, and almost 50 schools participate in these competitions and win multiple titles. Me, my friends, and our seniors also participate in these competitions and make our school proud by winning in them. My school has great teachers, excellent faculty and all the facilities that one student needs to excel in his/her life.</p>

            <p>The biggest reason behind the success and fame of my school is our Principal Sir. He is 50 years old, yet very active and disciplined. He has an attractive personality, and his knowledge is commendable. He loves all the students and always spare some time from his busy schedule to monitor the progress of all the students. I admire his personality and principles. He always encourages us to take part in various extracurricular activities, and it is the result of his support & dedication that the students of my school always perform better in all the competitions. We all are fortunate to have him as our Principal.</p>

            <p>Principal sir awards the students who participate and win in different cultural and sports events. He has a good memory and remembers the name of every child. The students feel proud when Principal sir calls them by their names.</p>

            <p>After each term, assessments are also being conducted in the school. The teachers prepare us for these assessments, and the results are also shared with the parents. The assessment is a good system as students understand the learning gaps and can work on them. The teachers are very helpful as they assist the students wherever it is required.</p>

            <p>I love my school and my school life. Every day I learn new things here and enjoy it with my friends. All my teachers love me and always support me. I have learned discipline and punctuality from my teachers, and it will help me a lot in my future. I am proud to be a student at this school, and I will always miss my school after leaving it. My school life has taught me many things and given me some best friends who will be my friends forever. I wish all the success to my school and love everything about my school life. </p>

            <p>The different experiences which the students get in school life help them to become better human beings. When they leave school, they are not just taking the memories with them, but they take many friends, a career and good manners for life. It is the first place where the students come out of their family circles and create a new one with their teachers and friends. </p>

            <p>One should also remember that everyone is not privileged enough to get an education. If one gets this opportunity, then be grateful and work towards being a better human being. Cherish your school life and stay motivated.
            </p>

            <h3>Thank You</h4>
                <h3>Routash kumar</h3>
        </div>
    </div>
</main>