<?php
include "./templates/common/admin/header.php";
include './libs/Users.php';
$db = new DB();
$message = "";
$USER = new Users();
$response = array();
if (!isset($_SESSION['id'])) {
    header('location:home.php');
}


if (isset($_POST['update'])) {
    $response =  $USER->Update($_REQUEST);
}

if (isset($_POST['leave'])) {
    $response =  $USER->Leave($_REQUEST);
}

$select = "SELECT * FROM users WHERE id= {$_GET['id']}";
$query = $db->conn->query($select);
$row = $query->fetch_assoc();

?>
<div class="container-fluid">
    <div class="row">
        <div class="col-md-2">
            <?php include "./templates/common/admin/sidebar.php"; ?>
        </div>
        <div class="col-md-10 card-outer">
            <section class="vh-100 gradient-custom ">
                <?php
                if (count($response) > 0) {
                    print_r($response);
                    if ($response['status'] == 'success') { ?>
                        <div class="alert alert-success mt-5" role="alert">
                            <?php
                            echo "successfully" ?>
                        </div>
                    <?php
                    } else { ?>
                <?php
                    }
                }
                ?>
                <div class="container h-50 mt-5">
                    <div class="row justify-content-center align-items-center h-100">
                        <div class="col-12 col-lg-9 col-xl-7">
                            <div class="card shadow-2-strong card-registration" style="border-radius: 15px;">
                                <div class="p-4 p-md-5">
                                    <form action="#" method="post">
                                        <h3 class="mb-4 pb-2 pb-md-0 mb-md-5">Update
                                            <?php echo $row['fname']
                                            ?></h3>
                                        <div class="row">
                                            <div class="col-md-6 mb-4">
                                                <div class="form-outline">
                                                    <label class="form-label" for="fname">First Name</label>
                                                    <input type="text" id="fname" name="fname" class="form-control form-control-lg" value="<?php echo $row['fname'] ?>">
                                                </div>
                                            </div>
                                            <div class=" col-md-6 mb-4">
                                                <div class="form-outline">
                                                    <label class="form-label" for="lastName">Last Name</label>
                                                    <input type="text" name="lname" id="lname" class="form-control form-control-lg" value="<?php echo $row['lname'] ?>" />
                                                </div>
                                            </div>
                                        </div>
                                        <div class="row">
                                            <div class="col-md-6 mb-4 d-flex align-items-center">

                                                <div class="form-outline datepicker w-100">
                                                    <label for="email" class="form-label">Email</label>
                                                    <input type="text" class="form-control form-control-lg" id="email" name="email" value="<?php echo $row['email'] ?>" />
                                                </div>
                                            </div>
                                            <div class="col-md-6 mb-4">
                                            </div>
                                        </div>
                                        <div class="row">

                                            <div class="col-md-6 mb-4 pb-2">
                                                <div class="form-outline">
                                                    <?php if ($_SESSION['user_role'] == "role_staff" || $_SESSION['user_role'] == "role_admin") {
                                                    ?>
                                                        <label for="#" class="form-label">Assign Class:</label><br>
                                                        <select name="class" class="form-control form-control-lg">
                                                            <option value="">Select Class</option>
                                                            <?php
                                                            foreach ($_SESSION['classes'] as $class) {
                                                                $selected = '';
                                                                if ($row['user_class'] == $class['id']) {
                                                                    $selected = 'selected';
                                                                }
                                                            ?>
                                                                <option value="<?php echo $class['id']; ?>" <?php echo $selected; ?>><?php echo $class['title']; ?></option>
                                                            <?php }
                                                            ?>
                                                        </select>
                                                    <?php } ?>

                                                </div>
                                            </div>

                                            <div class="col-md-6 mb-4 pb-2">
                                                <div class="form-outline">
                                                    <label class="form-label" for="phonenum">Phone Number</label>
                                                    <input type="tel" id="phone_Number" class="form-control form-control-lg" name="phone_Number" value="<?php echo $row['phone_Number'] ?>" />
                                                </div>
                                            </div>
                                        </div>
                                        <div class="row">
                                            <div class="col-md-6">
                                                <div class="ml-5 pb-3 w-100 ">
                                                    <input type="submit" name="update" class="btn btn-primary btn-lg mb-5" value="Update" />
                                                </div>
                                            </div>
                                            <div class="col-md-6">
                                                <div class="ml-5 pb-3 w-100">
                                                    <input type="submit" name="leave" class="btn btn-primary btn-lg mb-5" value="Leave" />
                                                </div>
                                            </div>
                                        </div>
                                    </form>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </section>
        </div>
    </div>
</div>