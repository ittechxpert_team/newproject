<?php
include './templates/common/admin/header.php';
include './libs/attendanse.php';
session_start();
$at = new Attend();
$response = array();
if (isset($_REQUEST['submit'])) {
    $at->userattendanse($_POST);
}
if (!isset($_SESSION['id'])) {
    header('location:home.php');
}

?>

<head>
    <style>
        .row {
            display: flex;
            -ms-flex-wrap: wrap;
            flex-wrap: wrap;
            margin-right: -15px;
            margin-left: -4px;
            justify-content: space-between;
        }

        .col-md-2 {
            -webkit-box-flex: 0;
            -ms-flex: 0 0 16.666667%;
            flex: 0 0 16.666667%;
            max-width: 15.9%;
        }
    </style>
</head>
<div class="container-fulid">
    <div class="row w-100">
        <div class="col-md-2"><?php include "./templates/common/admin/sidebar.php" ?></div>
        <div class="col-md-10">
            <?php
            if (count($response) > 0) {
                if ($response['status'] == 'success') { ?>
                    <div class="alert alert-success" id="success-alert">
                        <button type="button" class="close" data-dismiss="alert">x</button>
                        <strong>Success!</strong>
                    </div>
            <?php } else {
                }
            }
            ?>
            <form action="#" method="POST">
                <div class="row">

                    <?php
                    $sql = "SELECT * FROM `users` WHERE status= 1 AND user_class='{$_SESSION['user_class']}' AND user_role='role_student';";
                    $result = $db->select($sql);
                    while ($row = mysqli_fetch_assoc($result)) {
                        // $_SESSION['classes'][] = $row;
                        // echo "<pre>";
                        // print_r($_SESSION['classes'][] = $row);
                        // echo '</pre>';
                    ?><div class="col-md-2 mt-3">

                            <div class="card  text-black border border-dark  ">
                                <div class="card-body">
                                    <h4><?php echo $row['fname'] ?></h4>
                                    <div class="form-check form-check-inline">
                                        <input class="form-check-input attendance" id="present<?= $row['id']; ?>" type="radio" name="attend[<?= $row['id']; ?>][att]" value="Present" />
                                        <label for="present<?= $row['id']; ?>" class="form-check-label">Present</label>
                                    </div>
                                    <div class="form-check form-check-inline">
                                        <input class="form-check-input attendance" id="absent<?= $row['id']; ?>" type="radio" name="attend[<?= $row['id']; ?>][att]" value="Absent" />
                                        <label for="absent<?= $row['id']; ?>" class="form-check-label">Absent</label>
                                    </div>
                                    <div class="form-check form-check-inline">
                                        <input class="form-check-input attendance" id="leave<?= $row['id']; ?>" type="radio" name="attend[<?= $row['id']; ?>][att]" value="Leave" />
                                        <label for="leave<?= $row['id']; ?>" class="form-check-label">Leave</label>
                                    </div>
                                    <div class="form-outline leave-message" style="display:none;">
                                        <textarea name="attend[<?= $row['id']; ?>][comm]" class="form-control rounded-0" placeholder="Reason.." value="<?php echo $comment ?>" style="width: 100%"></textarea>
                                    </div>
                                </div>
                            </div>
                        </div><?php } ?>
                </div>
                <div class="btn"><input class="btn btn-primary " name="submit" type="submit" value="Add" />
                </div>
            </form>
        </div>
    </div>
</div>
<script>
    $('.attendance').change(function() {
        if ($(this).val() == 'Leave') {
            $(this).parents('.card-body').find('.leave-message').show();
        } else {
            $(this).parents('.card-body').find('.leave-message').hide();
        }
    })
</script>